# OER Tutorials

In this repository you can find tutorials for OER creation using Git.

### Create a course using GitHub Template
[View with LiaScript](https://liascript.github.io/course/?https://api.allorigins.win/raw?url=https://gitlab.com/smatts/oer-tutorials/-/raw/main/github_template/15-min-tutorial.md#1)